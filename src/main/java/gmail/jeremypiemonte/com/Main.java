package gmail.jeremypiemonte.com;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.StructureModifier;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    Main plugin;

    public boolean jsonHas(JsonElement json, String tag, String search) {
        return json.getAsJsonObject().get(tag).getAsString().contains(search);
    }
    public boolean jsonHas(JsonElement json, String tag) {
        return json.getAsJsonObject().has(tag);
    }

    @Override
    public void onEnable() {
        plugin = this;
        ProtocolManager manager = ProtocolLibrary.getProtocolManager();
        manager.addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL, PacketType.Play.Server.CHAT) {
            @Override
            public void onPacketSending(PacketEvent event) {
                Player player = event.getPlayer();
                PacketContainer packet = event.getPacket();

                if (event.getPacket().getChatComponents().getValues().get(0) == null) return;

                String jsonMessage = event.getPacket().getChatComponents().getValues().get(0).getJson();
                JsonParser parser = new JsonParser();
                JsonElement jsonTree = parser.parse(jsonMessage);

                JsonObject jsonObj = jsonTree.getAsJsonObject();
                if (!jsonObj.has("extra")) return;

                JsonArray jsonArr = jsonObj.getAsJsonArray("extra");

                boolean start = false;
                for (JsonElement element : jsonArr) {
                    if (!element.isJsonObject() || !jsonHas(element, "text")) continue;
                    if (jsonHas(element, "text", ":")) break;
                    if (jsonHas(element, "text", "~")) start = true;
                    if (!start) continue;
                    element.getAsJsonObject().add("hoverEvent", new JsonObject());
                    element.getAsJsonObject().get("hoverEvent").getAsJsonObject().addProperty("action", "show_text");
                    element.getAsJsonObject().get("hoverEvent").getAsJsonObject().addProperty("value", player.getName());

                }

                StructureModifier<WrappedChatComponent> chatComponents = packet.getChatComponents();
                WrappedChatComponent msg = chatComponents.read(0);

                msg.setJson(jsonTree.toString());
                chatComponents.write(0, msg);
            }
        });

        getLogger().info("HoverTownyChat loaded.");
    }
}
